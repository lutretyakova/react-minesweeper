export const BOARD_W = 20;

export const BOARD_H = 15;

export const CELL_W = 30;

export const CELL_H = 30;

export const MINES = 30;

export const OPENED = 2;

export const CLOSED = 1;

export const FLAGGED = -1;

export const MINE = -2;

export const NUMBER_COLORS = ['blue', 'green', 'tomato', 'darkblue', 'darkred', 'cyan', 'brown', 'pirple'];

export const WIN = 1;

export const DEFEAT = -1;

export const RUNNING = 0;

export const PAUSED = -2;

export const BEST_TIME_KEY = 'minesweeper-best';

export const LAST_TIME_KEY = 'minesweeper-last';

export const TIME_OPTIONS = {
    minute: '2-digit',
    second: '2-digit'
}