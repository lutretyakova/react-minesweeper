import React from 'react';
import { connect } from 'react-redux';
import TimerMixin from 'react-timer-mixin';
import { Link } from 'react-router-dom';

import * as actions from './actions.js';

import './App.css';
import Cell from './components/Cell.js';
import Modal from './components/Modal.js';
import Restart from './components/Restart.js';
import TimeCounter from './components/TimeCounter.js';
import MinesCounter from './components/MinesCounter.js';

import { BOARD_W, RUNNING, BOARD_H, CELL_W, PAUSED } from './constants.js';

const createReactClass = require('create-react-class');

const App = createReactClass({
    mixins: [TimerMixin],
    componentDidMount: function () {
        const { tick } = this.props;
        const fire = () => {
            this.setTimeout(() => {
                tick();
                fire();
            }, 100);
        }
        fire();
    },
    render() {
        let cells = [];
        for (let i = 0; i < BOARD_W * BOARD_H; i++) {
            cells.push(<Cell key={i} number={i} />);
        }

        const { state } = this.props;
        const modal = (state.gameStatus !== PAUSED &&
            state.gameStatus !== RUNNING) ? <Modal /> : null;

        const gridStyle = {
            display: 'grid',
            gridTemplateColumns: `repeat(${BOARD_W}, ${CELL_W}px)`,
            maxWidth: `${BOARD_W * CELL_W}px`,
        }

        return (
            <div className="App">
                <header className="header">
                    <div className="centred-header">
                        <h1>Minesweeper</h1>
                        <MinesCounter />
                        <TimeCounter />
                        <Restart />
                        <Link to="/score" className="score-link"><i className="fa fa-trophy"></i></Link>
                    </div>
                </header>

                <div style={gridStyle} className="grid">
                    {cells}
                </div>

                <footer>
                    <a href="https://gitlab.com/lutretyakova/react-minesweeper" className="source">
                        <i className="fa fa-gitlab"></i> Ссылка на исходный код
                    </a>
                </footer>
                {modal}
            </div>
        );
    }
});

export default connect((state) => ({ state: state }), actions)(App);
