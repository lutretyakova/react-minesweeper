import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux'
import { createStore } from 'redux';
import { HashRouter, Route } from 'react-router-dom';

import reducer from './reducer.js';
import './index.css';
import App from './App';
import ScorePage from './components/ScorePage';

import registerServiceWorker from './registerServiceWorker';

const store = createStore(reducer);

ReactDOM.render(
    <Provider store={store}>
        <HashRouter>
            <div>
                <Route exact path="/" component={App} />
                <Route path="/score" component={ScorePage} />
            </div>
        </HashRouter>
    </Provider>,
    document.getElementById('root')
);
registerServiceWorker();
