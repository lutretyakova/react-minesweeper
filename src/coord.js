import { BOARD_W } from './constants.js';

export const getGridIndexes = (lineInd) => {
    let x = lineInd % BOARD_W;
    let y = (lineInd - x) / BOARD_W;
    return { x, y }
}
