import { OPEN_TYPE, FLAG_TYPE, RESTART_GAME, TICK, PAUSE_TYPE } from './actionConstants.js';

export const openCell = (index) => ({
    type: OPEN_TYPE,
    index
});

export const setFlag = (index) => ({
    type: FLAG_TYPE,
    index
});

export const restartGame = () => ({
    type: RESTART_GAME
});

export const tick = () => ({
    type: TICK
});

export const togglePause = () => ({
    type: PAUSE_TYPE
})