import { OPEN_TYPE, FLAG_TYPE, RESTART_GAME, TICK, PAUSE_TYPE } from './actionConstants.js';
import { MINES, BOARD_W, BOARD_H, FLAGGED, DEFEAT, RUNNING, WIN, PAUSED } from './constants.js';
import { OPENED, CLOSED, MINE } from './constants.js';
import { getGridIndexes } from './coord.js';


const getMine = () => Math.floor(Math.random() * BOARD_W * BOARD_H);

const init = () => {
    let board = (new Array(BOARD_H)).fill(null);
    board = board.map(() => (new Array(BOARD_W)).fill(null));

    let status = (new Array(BOARD_H)).fill(null);
    status = status.map(() => (new Array(BOARD_W)).fill(CLOSED));

    for (let m = 0; m < MINES; m++) {
        while (true) {
            const mineInd = getMine();
            const { x, y } = getGridIndexes(mineInd);
            if (board[y][x] !== MINE) {
                board[y][x] = MINE;
                break;
            }
        }
    }

    for (let x = 0; x < BOARD_W; x++) {
        for (let y = 0; y < BOARD_H; y++) {
            if (board[y][x] === MINE) {
                continue;
            }
            let mines = 0;
            for (let dx = -1; dx <= 1; dx++) {
                for (let dy = -1; dy <= 1; dy++) {
                    const xi = x + dx,
                        yi = y + dy;
                    if (xi >= 0 && xi < BOARD_W && yi >= 0 && yi < BOARD_H) {
                        if (board[yi][xi] === MINE) {
                            mines++;
                        }
                    }
                }
            }
            board[y][x] = mines;
        }
    }

    const start = new Date();

    return {
        board,
        status,
        gameStatus: RUNNING,
        start: start,
        current: start,
        flagged: 0,
        exploded: null,
        timeOffset: 0
    }
}


const openAroundEmpty = (state, status, cellX, cellY) => {
    let ids = [{ x: cellX, y: cellY }];
    do {
        let { x, y } = ids.pop();
        for (let dx = -1; dx <= 1; dx++) {
            for (let dy = -1; dy <= 1; dy++) {
                const xi = x + dx,
                    yi = y + dy;
                if (xi >= 0 && xi < BOARD_W && yi >= 0 && yi < BOARD_H) {
                    if (status[yi][xi] === CLOSED && state.board[yi][xi] === 0 &&
                        status !== FLAGGED) {
                        ids.push({ x: xi, y: yi });
                    }
                    if (status[yi][xi] !== FLAGGED) {
                        status[yi][xi] = OPENED;
                    }
                }
            }
        }
    } while (ids.length > 0);

    return status
}


const gameOver = (state, cellX, cellY) => {
    let status = (new Array(BOARD_H)).fill(null);
    status = state.status.map((row) => (row.slice()));

    let minesInd = [];
    for (let x = 0; x < BOARD_W; x++) {
        for (let y = 0; y < BOARD_H; y++) {
            if (state.board[y][x] === MINE) {
                minesInd.push({ x, y });
            }
        }
    }

    for (let { x, y } of minesInd) {
        status[y][x] = OPENED;
    }

    return {
        ...state,
        status: status,
        gameStatus: DEFEAT,
        exploded: { x: cellX, y: cellY }
    }
}


const defineWin = (state, status) => {
    for (let x = 0; x < BOARD_W; x++) {
        for (let y = 0; y < BOARD_H; y++) {
            if (state.board[y][x] !== MINE && status[y][x] !== OPENED) {
                return false;
            }
        }
    }
    return true;
}


const openCell = (state, index) => {
    let status = (new Array(BOARD_H)).fill(null);
    status = state.status.map((row) => (row.slice()));

    const { x: cellX, y: cellY } = getGridIndexes(index);

    if (state.status[cellY][cellX] === CLOSED) {
        status[cellY][cellX] = OPENED;
        if (state.board[cellY][cellX] === MINE) {
            return gameOver(state, cellX, cellY);
        } else if (state.board[cellY][cellX] === 0) {
            status = openAroundEmpty(state, status, cellX, cellY);
        }
    }

    const gameStatus = defineWin(state, status) ? WIN : RUNNING;
    return {
        ...state,
        status: status,
        gameStatus: gameStatus
    };

}


const setFlag = (state, index) => {
    let status = (new Array(BOARD_H)).fill(null);
    status = state.status.map((row) => (row.slice()));

    const { x, y } = getGridIndexes(index);
    let flagged = state.flagged;
    if (status[y][x] !== OPENED) {
        status[y][x] = -status[y][x];
        flagged += status[y][x] === FLAGGED ? 1 : -1;
    }
    return {
        ...state,
        status: status,
        flagged: flagged
    }
}


const tick = (state) => {
    if (state.gameStatus !== RUNNING) {
        return state
    }

    const start = state.timeOffset === 0 ? state.start :
        new Date(state.start.getTime() + state.timeOffset);
    return {
        ...state,
        current: new Date(),
        start: start,
        timeOffset: 0
    }
}

const togglePause = (state) => {
    const gameStatus = state.gameStatus === PAUSED ? state.oldStatus : PAUSED;
    const timeOffset = gameStatus === PAUSED ? new Date() : new Date() - state.timeOffset;
    return {
        ...state,
        oldStatus: state.gameStatus,
        gameStatus: gameStatus,
        timeOffset: timeOffset
    }
}


export default (state = init(), action) => {
    switch (action.type) {
        case OPEN_TYPE:
            return openCell(state, action.index);
        case FLAG_TYPE:
            return setFlag(state, action.index);
        case RESTART_GAME:
            return init();
        case TICK:
            return tick(state);
        case PAUSE_TYPE:
            return togglePause(state);
        default:
            return state;
    }
}


