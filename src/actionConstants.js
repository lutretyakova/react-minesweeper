export const OPEN_TYPE = 'open';

export const FLAG_TYPE = 'flag';

export const RESTART_GAME = 'restart';

export const TICK = 'tick';

export const PAUSE_TYPE = 'pause';