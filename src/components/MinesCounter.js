import React from 'react';

import { connect } from 'react-redux';

import * as actions from '../actions.js';
import { MINES } from '../constants.js';

class MineCounter extends React.Component {
    render() {
        const { state } = this.props;
        return (
            <div><i className="fa fa-flag"></i> {state.flagged}/{MINES}</div>
        )
    }
}

export default connect((state) => ({ state: state }), actions)(MineCounter);