import React from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions.js';
import { DEFEAT } from '../constants';
import WinForm from './WinForm.js';

class Modal extends React.Component {
    render() {
        const { state } = this.props;
        const modalText = state.gameStatus === DEFEAT ? <h1>Вы проиграли</h1> : < WinForm />;

        return (
            <div className='modal'>
                {modalText}
            </div>
        )
    }
}

export default connect((state) => ({ state: state }), actions)(Modal);