import React from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions.js';

class EraseButton extends React.Component{
    eraseScore(){
        localStorage.clear();
        document.location.reload();
    }

    render() {
        return (
            <button className="erase" onClick={() => this.eraseScore()}>
                <i className="fa fa-trash-o"></i>    
            </button>
        )
    }
}

export default connect((state) => ({ state: state}), actions)(EraseButton);