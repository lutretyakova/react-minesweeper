import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import * as actions from '../actions.js';

import EraseButton from './EraseButton.js';
import { BEST_TIME_KEY, LAST_TIME_KEY, TIME_OPTIONS } from '../constants.js';

class ScorePage extends React.Component {
    getBestTime() {
        let bestTime = [];
        if (localStorage.getItem(BEST_TIME_KEY) !== null) {
            bestTime = JSON.parse(localStorage.getItem(BEST_TIME_KEY));
        }

        return bestTime.map(({ username, runtime }, i) => {
            const even = i % 2 === 0 ? 'even' : '';
            return (
                <div key={i + 1} className={`best-time ${even}`}>
                    <div>{i + 1}</div>
                    <div>
                        {new Date(runtime).toLocaleString('es-US', TIME_OPTIONS)}</div><div>{username}
                    </div>
                </div>
            );
        });
    }


    getLastGame() {
        let last = null;
        if (localStorage.getItem(LAST_TIME_KEY) !== null) {
            last = JSON.parse(localStorage.getItem(LAST_TIME_KEY));
        }

        if (last) {
            const { runtime, username } = last;
            return (
                <div>
                    <h3>Результат последней игры</h3>
                    <div className='best-time even'>
                        <div>~</div>
                        <div>
                            {new Date(runtime).toLocaleString('es-US', TIME_OPTIONS)}</div><div>{username}
                        </div>
                    </div>
                </div>
            );
        }
        return;
    }


    componentDidMount() {
        const { togglePause } = this.props;
        togglePause();
    }

    componentWillUnmount() {
        const { togglePause } = this.props;
        togglePause();
    }

    render() {
        const isHasData = localStorage.getItem(BEST_TIME_KEY);
        const eraseBtn = isHasData ? <EraseButton /> : null;

        let bestTime = <h3>Здесь пока пусто...</h3>;
        if (isHasData){
            bestTime = 
                <div>
                    <h3>Лучшие результаты</h3>
                    { this.getBestTime() }
                </div>
        }
        return (
            <div>
                <header className="header">
                    <div className="centred-header score-header">
                        <h1>
                            <Link to="/">Minesweeper</Link>
                        </h1>
                        { eraseBtn }
                        <Link to="/" className="back-link">
                            <i className="fa fa-arrow-left"></i>
                        </Link>
                    </div>
                </header>
                <div className="score">
                    { bestTime }
                    {this.getLastGame()}
                </div>
                <footer>
                    <a href="https://gitlab.com/lutretyakova/react-minesweeper" className="source">
                        <i className="fa fa-gitlab"></i> Ссылка на исходный код
                    </a>
                </footer>
            </div>
        )
    }
}

export default connect((state) => ({ state: state }), actions)(ScorePage);