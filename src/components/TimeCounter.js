import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions.js';
import { TIME_OPTIONS } from '../constants';

class TimeCounter extends Component {
    render() {
        const { state } = this.props;
        const runTime = new Date(state.current - state.start);
        const timer = runTime.toLocaleString('en-US', TIME_OPTIONS);
        return (
            <div id='timer'><i className="fa fa-clock-o"></i> {timer}</div>
        )
    }
}

export default connect((state) => ({ state: state }), actions)(TimeCounter);