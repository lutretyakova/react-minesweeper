import React from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions.js';
import { BEST_TIME_KEY, LAST_TIME_KEY } from '../constants.js';
const SCORE_LENGHT = 10;

class WinForm extends React.Component {
    saveTime() {
        let bestTime = [];
        if (localStorage.getItem(BEST_TIME_KEY) !== null) {
            bestTime = JSON.parse(localStorage.getItem(BEST_TIME_KEY));
        }
        const { state } = this.props;
        const record = {
            username: this.state.username,
            runtime: new Date(state.current - state.start)
        }
        bestTime.push(record);
        bestTime.sort((a, b) => (
            new Date(a['runtime']) - new Date(b['runtime'])
        ));

        bestTime = bestTime.slice(0, SCORE_LENGHT);

        localStorage.setItem(LAST_TIME_KEY, JSON.stringify(record));
        localStorage.setItem(BEST_TIME_KEY, JSON.stringify(bestTime));
    }


    constructor(props) {
        super(props);
        this.state = { username: '' };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChange(event) {
        this.setState({ username: event.target.value });
    }


    handleSubmit(event) {
        event.preventDefault();
        this.saveTime();

        const { restartGame } = this.props;
        restartGame();
    }


    render() {
        return (
            <div className="win-modal">
                <header>Поздравляем с победой!!!</header>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Как Вас зовут?
                        <input type="text" value={this.state.username} onChange={this.handleChange} />
                    </label>
                    <input type="submit" value="OK" className="btn" />
                </form>
            </div>
        )

    }
}

export default connect((state) => ({ state: state }), actions)(WinForm);