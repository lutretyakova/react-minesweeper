import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions.js';


class Restart extends Component {
    handleClick() {
        const { restartGame } = this.props;
        restartGame();
    }

    render() {
        return (
            <button onClick={() => this.handleClick()} className='restart'>
                <i className="fa fa-refresh"></i> 
            </button>
        )
    }
}

export default connect((state) => ({ state: state }), actions)(Restart);