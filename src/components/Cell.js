import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

import { getGridIndexes } from '../coord.js';
import { CELL_W, CELL_H, OPENED, FLAGGED, NUMBER_COLORS, MINE } from '../constants.js';

class Cell extends Component {
    handleClickCell(number) {
        const { openCell } = this.props;
        openCell(number);
    }

    handleFlagged(e, number) {
        const { setFlag } = this.props;
        setFlag(number);
        e.preventDefault();
    }

    render() {
        const { number, state } = this.props;
        const { x, y } = getGridIndexes(number);
        const content = (state.board[y][x] === 0 ||
            state.status[y][x] !== OPENED ||
            state.board[y][x] === MINE) ? ' ' : state.board[y][x];

        let cellCss = 'cell-closed';
        if (state.status[y][x] === OPENED) {
            cellCss = 'cell-opened';
            const isCellExploded = state.exploded !== null &&
                x === state.exploded['x'] && y === state.exploded['y'];
            if (isCellExploded) {
                cellCss += ' exploded';
            }
            if (state.board[y][x] === MINE) {
                cellCss += ' mine';
            }
        } else if (state.status[y][x] === FLAGGED) {
            cellCss = 'cell-opened cell-flagged';
        }

        const cellStyle = {
            width: `${CELL_W}px`,
            height: `${CELL_H}px`,
            lineHeight: `${CELL_H}px`,
            color: NUMBER_COLORS[state.board[y][x] - 1]
        }
        return (
            <div style={cellStyle} className={cellCss}
                onContextMenu={(e) => this.handleFlagged(e, number)}
                onClick={() => this.handleClickCell(number)}>
                {content}
            </div>
        );
    }
}

export default connect((state) => ({ state: state }), actions)(Cell);